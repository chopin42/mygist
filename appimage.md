# How to convert a Python file into a AppImage :dvd:

1. Install AppImageTool :wrench: :

```shell
wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
mv appimagetool-x86_64.AppImage /usr/bin/appimagetool
```

2. Create an empty folder with your Python script inside :file_folder:

```shell
mkdir AppDir
cd AppDir
cp /path/to/your/script.py .
```

3. Create a file called `AppRun`

```shell
#!/bin/sh
cd "$(dirname "$0")"
exec python3 <your file> $1 $2 $3 $4 $5
```

4. Add an icon for your app, you can name the icon `icon.png`

5. Create a file called by the name of your app.desktop (example: `qBittorrent.desktop`)

```shell
[Desktop Entry]
Type=Application
Name=<name of your app>
Icon=icon
Categories=<category>;
```

6. Run the following command to generate your image:

```shell
chmod +x *
appimagetool .
```
