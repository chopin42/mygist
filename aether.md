This is the notes I've taken from the following video https://www.youtube.com/watch?v=A9zL2csO7t8 as well as time codes.

Value of a data is contextual using the upvotes, without block-chain. In aether there is no blocks but there is a flow.

Active discussions cost more. Only 2 weeks of data are always downloaded, after 6 months, the data can start to be deleted.

Every user can choose the space disk allocated for these purposes.



6 objects are defined and act together



Option 1: Everybody has a copy of the network, nodes keeps data that they are not interested in. Also the nodes needs a lot of space.

Option 2: Only interested things are shared like a pie between nodes. The problems are that finding content is tricky and slow, this require centralization to be like "Super-Nodes" to be faster and get discovery easier. Also you cannot access to data you didn't discovered yet.

Option Aether: AS long as you have enough space to get network, you get all the data, if not it will select the data.

What happen when a thread disappear, when  a new post will comes up under the missing data, the node that posted the comment will automatically share the whole graph with the network (see later to see how this works) so the tree is completed and synced again.

So what happen when a mod becomes non-mod, if a part of the network receive this action too late, and the non-mod deletes a post, the delayed network part will delete the data. To solve this issue in Aether, the data is going to be synced again by the part who still have this data and revert the delete action.

To share the data with the other nodes there is few approachs:

The naive approach: Randomly share the data to all the users.  This is very unpredictable but this is good when there is only a very few number of nodes.

The neighborhoods, more complex but  more stable and predictable. The principle is the following:

1. There is a neighborhood of 10 nodes
2. A neighborhood is hit every minute
3. The oldest node is getting out after 10 minutes and a new one is getting in to keep the same number.

To fight again spam, there is a proof-of-work function that will be required for every action, this will limit the spam.

For moderation, there are few things.
First the mods are getting elected and impeached, mod's actions are reversible, the users choose their own mods list  with the ones they trust or not. (optional)

For the low-end devices such as mobile users, they are just front-ends so they need to be connected to a back-end to be able to work.

The Zooko's triangle is the following: human-meaningful, decentralized, secure. These are the options:

Participants can choose names that are the same as each other (insecure)

Participants can use fingerprint hashes (non-human-meaningful)

Participants can rely on a common shared authority (non-decentralized)

In Aether, everybody has a non-unique username, this is just a text field on the public key.

For people who wants a **unique** username they can get an optional *Cerificate of authority* that works the same way as traditional web CAs unique domain names with fees.

It's also a way to say "I support this project" but they are not key features.


## Timecodes

00:00 Introduction

8:14 Persistence, pick whether you want your data permanent or ephemeral based on the values of your old data

11:03 Persistence, How long do you want the data around?

13:06 Graph and topology, what are your nodes and vertices? (objects and actions)

14:54 Data distribution, who gets a copy of which data?

18:05 Edge cases, missing data

19:52 Edge cases, delayed data

22:15 Networking, how do your nodes find each other and at what interval?

24:25 Application level concerns, these are specific to your app but here are a few you will probably encounter (spam, moderation, low-end devices, unique names)

28:50 The end